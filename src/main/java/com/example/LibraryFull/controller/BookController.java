package com.example.LibraryFull.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.mail.MessagingException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.LibraryFull.request.BookRequest;
import com.example.LibraryFull.response.ApiResponse;
import com.example.LibraryFull.response.AuthorResponse;
import com.example.LibraryFull.response.BookResponse;
import com.example.LibraryFull.response.PagedResponse;
import com.example.LibraryFull.security.CurrentUser;
import com.example.LibraryFull.security.UserPrincipal;
import com.example.LibraryFull.service.AuthorService;
import com.example.LibraryFull.service.BookService;
import com.example.LibraryFull.service.BorrowService;
import com.example.LibraryFull.util.AppConstants;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

@RestController
@Api(tags = "Book Endpoint",value = "Book Management System", description = "Operations pertaining to book in Library Management System")
@RequestMapping(path = "/books",produces = { MediaType.APPLICATION_JSON_VALUE })
@Validated 
@Slf4j
public class BookController {
	
	@Autowired
	BookService bookService;
	
	@Autowired
	BorrowService borrowService;
	
	
	
	@ApiOperation(value = "Find all books",notes = "Return all books")
	@GetMapping
	public ResponseEntity<PagedResponse<BookResponse>> getAuthors(
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) throws MessagingException {
		
		return ResponseEntity.ok(bookService.getBooks(page, size));
		
	}
	
	// -------------------Create a Book-------------------------------------------
	@ApiOperation(value = "Create new book", notes = "Creates new  book. Returns created book with id.",response = BookResponse.class)
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BookResponse> createBook(
    		@ApiParam(name = "Book", value = "JSON book to create", required = true)
    		@Valid @RequestBody BookRequest bookRequest)  {
		
		log.info("inside method create() ");    
		
        return ResponseEntity.status(HttpStatus.CREATED).body(bookService.createBook(bookRequest));
      
    }
	
	@ApiOperation(value = "Get book by id", notes = "Returns book for id specified.",response = BookResponse.class)
	@GetMapping("/{bookId}")
	public ResponseEntity<BookResponse> getBook(
			@PathVariable(value = "bookId") Long bookId) {
		
		return ResponseEntity.ok(bookService.getBookById(bookId));
	}
	
	@ApiOperation(value = "Find all books Availables",notes = "Return all books availables")
	@GetMapping("/availables")
	public ResponseEntity<PagedResponse<BookResponse>> getBooksAvailables(
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) throws MessagingException {
		
		return ResponseEntity.ok(bookService.getBooksAvailables(true,page, size));
		
	}
	
	@ApiOperation(value = "Borrow a book", notes = "Borrow a book",response = ApiResponse.class)
	@PostMapping(path = "/{bookId}/borrows")
    public ResponseEntity<ApiResponse> createBorrow(@CurrentUser UserPrincipal currentUser,@ApiParam(value = "id of the book", required = true) @PathVariable(value = "bookId") Long bookId) throws MailException, MessagingException  {
		log.info("inside method createBorrowt() ");    
		
        return  ResponseEntity.ok(borrowService.createBorrow(bookId,currentUser));
    }
}
