package com.example.LibraryFull.controller;

import javax.mail.MessagingException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.LibraryFull.service.AuthorService;
import com.example.LibraryFull.service.MailService;
import com.example.LibraryFull.request.AuthorRequest;
import com.example.LibraryFull.response.AuthorResponse;
import com.example.LibraryFull.response.BookPlainResponse;
import com.example.LibraryFull.response.EditorialResponse;
import com.example.LibraryFull.response.PagedResponse;
import com.example.LibraryFull.response.PagedResponseWithEntity;
import com.example.LibraryFull.util.AppConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;


@Api(tags = "Author Endpoint",value = "Author Management System", description = "Operations pertaining to author in Library Management System")
@RestController
@RequestMapping(path = "/authors",produces = { MediaType.APPLICATION_JSON_VALUE })
@Validated 
public class AuthorController {
	
	@Autowired
	AuthorService authorService;
	
	
	@GetMapping
	public ResponseEntity<PagedResponse<AuthorResponse>> getAuthors(
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) throws MessagingException {
		
		return ResponseEntity.ok(authorService.getAuthors(page, size));
		
	}
	
	@GetMapping("/{authorId}")
	public ResponseEntity<AuthorResponse> getAuthor(
			@PathVariable(value = "authorId") Long authorId) {
		
		return ResponseEntity.ok(authorService.getAuthorById(authorId));
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Count the number of entities associated with resource name. This operation does not requires any role." ,authorizations = { @Authorization(value="Bearer") })
	public ResponseEntity<AuthorResponse> createAuthor(@Valid @RequestBody AuthorRequest authorRequest){
		
		return ResponseEntity.status(HttpStatus.CREATED).body(authorService.createAuthor(authorRequest));
	}
	
	@PutMapping(path = "/{authorId}",consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Update a author",notes = "Update author by id and return it")
	public ResponseEntity<AuthorResponse> updateAuthor(@Valid @RequestBody AuthorRequest authorRequest, @PathVariable(value = "authorId") Long authorId){
		
		return ResponseEntity.status(HttpStatus.CREATED).body(authorService.updateAuthor(authorRequest, authorId));
	}
	/*
	@ApiOperation(value = "Find all books by author",notes = "Retrieve All Books By author")
	@GetMapping("/{authorId}/books")
	public ResponseEntity<PagedResponseWithEntity<AuthorResponse,BookPlainResponse>> getBooksByAuthor(
			@PathVariable(value = "authorId") Long authorId,
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {

		return ResponseEntity.ok(authorService.getBooksByAuthor(authorId,page, size));
		
	}
	*/
}
