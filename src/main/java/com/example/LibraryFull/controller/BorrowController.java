package com.example.LibraryFull.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.LibraryFull.response.BorrowResponse;
import com.example.LibraryFull.response.PagedResponse;
import com.example.LibraryFull.service.BorrowService;
import com.example.LibraryFull.util.AppConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

@RestController
@Api(tags = "Borrow Endpoint",value = "Borrow Management System", description = "Operations pertaining to borrow in Library Management System")
@RequestMapping(path = "/borrows",produces = { MediaType.APPLICATION_JSON_VALUE })
@Validated
public class BorrowController {
	
	@Autowired
	BorrowService borrowService;
	
	@ApiOperation(value = "Find all Editorials",notes = "Return all Editorials",authorizations = { @Authorization(value="Bearer") })
	@GetMapping
	public ResponseEntity<PagedResponse<BorrowResponse>> getBorrowsByStatus(
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {

		return ResponseEntity.ok(borrowService.getBorrowByStatus(page, size));
		
	}
}
