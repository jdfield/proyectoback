package com.example.LibraryFull.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.LibraryFull.entity.Author;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {
	Set<Author> findByIdIn(Set<Long> ids);
}
