package com.example.LibraryFull.repository;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.LibraryFull.entity.Borrow;
import com.example.LibraryFull.entity.StatusBorrow;


@Repository
public interface BorrowRepository extends JpaRepository<Borrow, Long> {
	
	@Transactional
	@Modifying(clearAutomatically = true)
    @Query("UPDATE #{#entityName} b SET b.status = :newStatus WHERE b.status = :status")
    int setStatusBorrows(@Param("newStatus") StatusBorrow newStatus,@Param("status") StatusBorrow status);
	
	Set<Borrow> findByReceptionDateBetweenAndStatus(Date startDate, Date endDate,StatusBorrow status);
}
