package com.example.LibraryFull.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.LibraryFull.entity.Editorial;


@Repository
public interface EditorialRepository extends JpaRepository<Editorial, Long> {
}
