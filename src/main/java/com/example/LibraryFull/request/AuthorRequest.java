package com.example.LibraryFull.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


import lombok.Data;

@Data
public class AuthorRequest {
	
	@NotNull
    @Size(min = 3, max = 100)
    private String name;
}
