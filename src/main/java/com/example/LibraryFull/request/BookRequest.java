package com.example.LibraryFull.request;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "Book DTO Creation", description = "Model to create a new Book")
public class BookRequest {
	
	@NotNull(message = "Required")
    @Size(min = 4, message = "Title of tag should have at least 4 characters")
	@ApiModelProperty(notes = "Title of the editorial", example = "Norma",position = 1)
	private String title;
	
	@Size(min = 20, message = "Name of tag should have at least 20 characters")
	@ApiModelProperty(notes = "Description", example = "La editorial colombiana Norma se especializa en literatura infantil y escolar.",position = 2)
	private String description;
	
	@ApiModelProperty(notes = "Editorial id", example = "1",position = 3)
	private Long editorialId;
	
	@ApiModelProperty(notes = "Author's ids", example = "[2,3]",position = 4)
	private Set<Long> authorIds = new HashSet<>();
}
