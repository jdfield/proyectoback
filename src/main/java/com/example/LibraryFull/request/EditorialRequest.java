package com.example.LibraryFull.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "Editorial DTO Creation", description = "Model to create a new Editorial")
public class EditorialRequest {

	@NotNull(message = "{name.NotNull}")
    @Size(min = 4, message = "Name of tag should have at least 4 characters")
	@ApiModelProperty(notes = "Name of the editorial", example = "Norma",position = 1)
	private String name;
	
	@Size(min = 20, message = "Name of tag should have at least 20 characters")
	@ApiModelProperty(notes = "Description", example = "La editorial colombiana Norma se especializa en literatura infantil y escolar.",position = 2)
	private String description;
	
	@NotNull(message = "Required")
	@ApiModelProperty(notes = "Year fundation ", example = "1960",position = 3)
	private int fundation;
	
	@NotNull(message = "Required")
	@ApiModelProperty(notes = "Website", example = "www.norma.com.co",position = 4)
	private String website;
}

