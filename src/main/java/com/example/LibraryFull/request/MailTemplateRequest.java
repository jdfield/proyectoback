package com.example.LibraryFull.request;

import java.util.List;

import org.thymeleaf.context.Context;


import lombok.Data;

@Data
public class MailTemplateRequest {
	
	private String to;
	private String[] bcc;
	private String subject;
	private Context context;
	private String template;
	
}
