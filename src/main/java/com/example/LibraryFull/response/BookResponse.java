package com.example.LibraryFull.response;

import java.util.Set;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "Book DTO", description = "Book Editorial")
public class BookResponse {
	
	@ApiModelProperty(notes = "Id of the book", example = "1",position = 1)
	private Long id;
	@ApiModelProperty(notes = "Tittle of the book", example = "Norma",position = 2)
	private String title;
	@ApiModelProperty(notes = "Description", example = "La editorial colombiana Norma se especializa en literatura infantil y escolar.",position = 3)
	private String description;
	@ApiModelProperty(notes = "Book is available", example = "true",position = 4)
	private Boolean available;
	@ApiModelProperty(notes = "Editorial", example = "Norma" ,position = 5)
	private EditorialResponse editorial;
	@ApiModelProperty(notes = "authors", example = "ejemplo" ,position = 6)
	private Set<AuthorResponse> authors ;
}
