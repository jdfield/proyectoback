package com.example.LibraryFull.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@ApiModel(value = "Editorial DTO", description = "Response Editorial")
public class EditorialResponse {

	@ApiModelProperty(notes = "Id of the editorial", example = "1",position = 1)
	private Long id;
	@ApiModelProperty(notes = "Name of the editorial", example = "Norma",position = 2)
	private String name;
	@ApiModelProperty(notes = "Description", example = "La editorial colombiana Norma se especializa en literatura infantil y escolar.",position = 3)
	private String description;
	@ApiModelProperty(notes = "Year fundation", example = "1960",position = 4)
	private int fundation;
	@ApiModelProperty(notes = "Website", example = "www.norma.com.co",position = 5)
	private String website;
	
}
