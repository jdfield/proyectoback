package com.example.LibraryFull.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class PagedResponseWithEntity<T,C> {
	private T entity;
	private List<C> content;
    private int page;
    private int size;
    private long totalElements;
    private int totalPages;
    private boolean last;

}
