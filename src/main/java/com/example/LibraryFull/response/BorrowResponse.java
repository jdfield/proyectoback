package com.example.LibraryFull.response;

import java.util.Date;

import com.example.LibraryFull.entity.StatusBorrow;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "Borrow DTO", description = "Response Borrow")
public class BorrowResponse {
	
	@ApiModelProperty(notes = "Id of the Borrow", example = "1",position = 1)
	private Long id;
	
	@ApiModelProperty(notes = "Borrow book",position = 2)
	private BookPlainResponse book;
	
	@ApiModelProperty(notes = "User",position = 3)
	private UserSummary user;
	
	@JsonFormat(pattern="dd/MM/yyyy")
	@ApiModelProperty(notes = "Reception Date",position = 4)
	private Date receptionDate;
	
	@ApiModelProperty(notes = "Delivery Date",position = 5)
	private Date deliveryDate;
	
	@ApiModelProperty(notes = "Created Date",position = 6)
	private Date createdAt;
	
	@ApiModelProperty(notes = "Satus",position = 7)
	private StatusBorrow status;
	
	
	
}
