package com.example.LibraryFull.response;

import java.util.List;
import java.util.Set;

import lombok.Data;

@Data
public class BookAuthorsResponse extends BookPlainResponse {

	private List<AuthorPlainResponse> authors ;
}
