package com.example.LibraryFull.service;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.example.LibraryFull.request.MailTemplateRequest;

@Service
public class MailService {

	/*
	 * The Spring Framework provides an easy abstraction for sending email by using
	 * the JavaMailSender interface, and Spring Boot provides auto-configuration for
	 * it as well as a starter module.
	 */
	private JavaMailSender javaMailSender;

	/**
	 * 
	 * @param javaMailSender
	 */
	@Autowired
	public MailService(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}
	
	@Autowired
    private TemplateEngine templateEngine;

	/**
	 * This function is used to send mail without attachment.
	 * @param user
	 * @throws MailException
	 */

	public void sendEmail(String email) throws MailException {

		/*
		 * This JavaMailSender Interface is used to send Mail in Spring Boot. This
		 * JavaMailSender extends the MailSender Interface which contains send()
		 * function. SimpleMailMessage Object is required because send() function uses
		 * object of SimpleMailMessage as a Parameter
		 */

		SimpleMailMessage mail = new SimpleMailMessage();
		mail.setTo(email);
		mail.setSubject("Testing Mail API");
		mail.setText("Hurray ! You have done that dude...");

		/*
		 * This send() contains an Object of SimpleMailMessage as an Parameter
		 */
		javaMailSender.send(mail);
	}

	/**
	 * This fucntion is used to send mail that contains a attachment.
	 * 
	 * @param user
	 * @throws MailException
	 * @throws MessagingException
	 */
	public void sendEmailWithAttachment(String email) throws MailException, MessagingException {

		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);

		helper.setTo(email);
		helper.setSubject("Testing Mail API with Attachment");

		ClassPathResource classPathResource = new ClassPathResource("payment-2019-08-21.pdf");
		System.out.println(classPathResource.getPath());
		helper.addAttachment(classPathResource.getFilename(), classPathResource);

		javaMailSender.send(mimeMessage);
	}
	
	public void sendEmailWithTemplate(String email) throws MailException, MessagingException{
		
		Context context = new Context();
		context.setVariable("title", "El titulo");
		context.setVariable("description", "Lorem Lorem Lorem");
		String body = templateEngine.process("email/template-1", context);
		//String body = "<h1>Hola mundo </h1>";
		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setTo(email);
        helper.setSubject("Testing Mail API with HTML");
        helper.setText(body, true);
		

		javaMailSender.send(mimeMessage);
	}
	
	public void sendEmailWithTemplate(MailTemplateRequest mailTemplateRequest) throws MailException, MessagingException{
	
		String body = templateEngine.process("email/"+mailTemplateRequest.getTemplate(), mailTemplateRequest.getContext());
		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
        helper.setTo(mailTemplateRequest.getTo());
        if (mailTemplateRequest.getBcc() != null) {
        	helper.setBcc(mailTemplateRequest.getBcc());
		}   
        helper.setSubject(mailTemplateRequest.getSubject());
        helper.setText(body, true);
        helper.addInline("logo", new ClassPathResource("logo.png"));
        helper.addInline("facebook", new ClassPathResource("facebook.png"));
        helper.addInline("twitter", new ClassPathResource("twitter.png"));
		javaMailSender.send(mimeMessage);
	}
	
	@Async
	public void sendEmailAsync() throws MailException, MessagingException {

		for(int i = 1; i < 8; i++) {
		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage,true);

		helper.setTo(InternetAddress.parse("xlmaster_juandavid@hotmail.com,juan.zapata@unillanos.edu.co"));
		helper.setBcc("juan.cuero@unillanos.edu.co");
		helper.setSubject("Testing Mail API Spring with Async and File Jajaja xD");
		helper.setText("Hurray ! You have done that dude... JAJAJA ES UNA PRUEBA :V",true);
		ClassPathResource classPathResource = new ClassPathResource("payment-2019-08-21.pdf");
		System.out.println(classPathResource.getPath());
		helper.addAttachment(classPathResource.getFilename(), classPathResource);

		javaMailSender.send(mimeMessage);
		}
		
	}
	

	//@Scheduled(cron = "0 35 8 * * ? ")
	public void sendEmailCron() throws MailException, MessagingException{
		
		System.out.println("enviando....");
		Context context = new Context();
		context.setVariable("title", "El titulo");
		context.setVariable("description", "Jajaja ya entendí, es que lo estaba llamando y la vaina es que esto se ejecuta solo");
		String body = templateEngine.process("email/template-1", context);
		//String body = "<h1>Hola mundo </h1>";
		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setTo(InternetAddress.parse("juan.hortua@unillanos.edu.co,juan.cuero@unillanos.edu.co"));
		helper.setBcc("juan.cuero@unillanos.edu.co");
        //helper.setTo("juan.cuero@unillanos.edu.co");
        helper.setSubject("Testing Mail API with HTML. a las 11:25 AM");
        helper.setText(body, true);
		

		javaMailSender.send(mimeMessage);
	}

}