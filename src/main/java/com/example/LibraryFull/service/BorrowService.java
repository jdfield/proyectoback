package com.example.LibraryFull.service;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.lang.reflect.Type;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.mail.MessagingException;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.data.domain.Sort;
import org.springframework.mail.MailException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

import com.example.LibraryFull.controller.BookController;
import com.example.LibraryFull.entity.Book;
import com.example.LibraryFull.entity.Borrow;
import com.example.LibraryFull.entity.StatusBorrow;
import com.example.LibraryFull.exception.BadRequestException;
import com.example.LibraryFull.repository.BookRepository;
import com.example.LibraryFull.repository.BorrowRepository;
import com.example.LibraryFull.request.MailTemplateRequest;
import com.example.LibraryFull.response.ApiResponse;
import com.example.LibraryFull.response.BookPlainResponse;
import com.example.LibraryFull.response.BookResponse;
import com.example.LibraryFull.response.BorrowResponse;
import com.example.LibraryFull.response.PagedResponse;
import com.example.LibraryFull.response.UserSummary;
import com.example.LibraryFull.security.UserPrincipal;
import com.example.LibraryFull.util.AppConstants;
import com.example.LibraryFull.util.DateUtility;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BorrowService {
	
	@Autowired
    private BorrowRepository borrowRepository;
	
	@Autowired
	BookService bookService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	MailService mailService;
	
	@Autowired
	ModelMapper modelMapper;
	
	public ApiResponse createBorrow(Long bookId,UserPrincipal currentUser) throws MailException, MessagingException {
		Date now = new Date();
		System.out.println(now);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(now);
		int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		/*
		if (dayOfWeek == 1 || dayOfWeek == 7) {
			return new ApiResponse(false, "Sorry, we don't work on weekends.");
		}
		
		if (hourOfDay >= 17) {
			return new ApiResponse(false, "Sorry, we work until 5:00 PM.");
		}
		*/
		BookResponse bookResponse = bookService.getBookById(bookId);
		if (!bookResponse.getAvailable()) {
			return new ApiResponse(false, "The book "+bookResponse.getTitle()+" is not available.");
		}
		
		Book book = modelMapper.map(bookResponse, Book.class);
		Borrow borrow = new Borrow();
		borrow.setBook(book);
		borrow.setReceptionDate(DateUtility.getNextDayEnabled(now, AppConstants.MAX_DAYS_RECEPTION));
		Collection<Long> book_id_list = new ArrayList<Long>();
		book_id_list.add(book.getId());
		bookService.setAvailableInBooks(false,book_id_list);
		borrowRepository.save(borrow);
		//CREAR UTIL
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");  
		System.out.println(borrow.getReceptionDate().toString());
        String date = dateFormat.format(borrow.getReceptionDate());  
		sendNotificationRequest(currentUser.getEmail(), currentUser.getName() ,bookResponse.getTitle(), date);
		return new ApiResponse(true, "Your request has been processed correctly. Come to the library as soon as possible to claim the book.");
	}
	
	
	public PagedResponse<BorrowResponse> getBorrowByStatus(int page, int size) {
        validatePageNumberAndSize(page, size);

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "id");
        Page<Borrow> borrows = borrowRepository.findAll(pageable);

        if (borrows.getNumberOfElements() == 0) {
            return new PagedResponse<>(Collections.emptyList(), borrows.getNumber(),
            		borrows.getSize(), borrows.getTotalElements(), borrows.getTotalPages(), borrows.isLast());
        }

        Type listType = new TypeToken<List<BorrowResponse>>(){}.getType();
		//List<BorrowResponse> borrowResponses = modelMapper.map(borrows.getContent(),listType);
		List<BorrowResponse> borrowResponses = new ArrayList<BorrowResponse>();
		
		borrows.getContent().forEach(q -> borrowResponses.add(convert(q)));

        return new PagedResponse<>(borrowResponses, borrows.getNumber(),
        		borrows.getSize(), borrows.getTotalElements(), borrows.getTotalPages(), borrows.isLast());
    }
	
	
	
	@Scheduled(cron = "${release-borrow-cron}")
	public void releaseBorrowAutomatically() {
		log.info("inside method releaseBorrowAutomatically() "); 
		borrowRepository.setStatusBorrows(StatusBorrow.CANCELLED,StatusBorrow.PENDING);
		bookService.setAvailableBooks(true, false);
	}
	
	@Scheduled(cron = "${send-delivery-reminder-cron}")
	public void sendDeliveryReminder() throws ParseException, MailException, MessagingException {
		//DRY.......................................................
		log.info("inside method sendDeliveryReminder() "); 
		String sDate1="12/09/2019";  
	    Date startDate=new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
	    Date endDate=new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
	    
		Set<Borrow> borrowSet = borrowRepository.findByReceptionDateBetweenAndStatus(startDate, endDate,StatusBorrow.APPROVED);
		List<Borrow> borrows = new ArrayList<>(borrowSet);	
		Set<Long> userIds = borrows.stream()
                .map(b -> b.getCreatedBy())
                .collect(Collectors.toSet());
		
		Set<UserSummary> users = userService.getUserInId(userIds);
		
		String[] bcc = users.stream()
				.map(b -> b.getEmail())
				.toArray(String[]::new);
		
		
		MailTemplateRequest mailTemplateRequest = new MailTemplateRequest();
		mailTemplateRequest.setTo("juan.cuero.pruebas@gmail.com");
		mailTemplateRequest.setBcc(bcc);
		mailTemplateRequest.setSubject("Reminder - Library Spring Boot REST API");
		Context context = new Context();
		context.setVariable("description","Remember to deliver the book today before 5:30 PM to avoid fines.");
		mailTemplateRequest.setContext(context);
		mailTemplateRequest.setTemplate("template-1");
		mailService.sendEmailWithTemplate(mailTemplateRequest);
		System.out.println("termina");

	}
	
	
	@Scheduled(cron = "${send-pickup-reminder-cron}")
	public void sendPickupReminder() throws ParseException, MailException, MessagingException {
		//DRY.......................................................
		log.info("inside method sendDeliveryReminder() "); 
		String sDate1="13/09/2019";  
	    Date startDate=new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
	    Date endDate=new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
	    //cambiar a created
		Set<Borrow> borrowSet = borrowRepository.findByReceptionDateBetweenAndStatus(startDate, endDate,StatusBorrow.PENDING);
		List<Borrow> borrows = new ArrayList<>(borrowSet);	
		Set<Long> userIds = borrows.stream()
                .map(b -> b.getCreatedBy())
                .collect(Collectors.toSet());
		
		Set<UserSummary> users = userService.getUserInId(userIds);
		
		String[] bcc = users.stream()
				.map(b -> b.getEmail())
				.toArray(String[]::new);
		
		
		MailTemplateRequest mailTemplateRequest = new MailTemplateRequest();
		mailTemplateRequest.setTo("juan.cuero.pruebas@gmail.com");
		mailTemplateRequest.setBcc(bcc);
		mailTemplateRequest.setSubject("Reminder - Library Spring Boot REST API");
		Context context = new Context();
		context.setVariable("description","Remember to go to the library for the book you requested before 5:30 PM.");
		mailTemplateRequest.setContext(context);
		mailTemplateRequest.setTemplate("template-1");
		mailService.sendEmailWithTemplate(mailTemplateRequest);
		System.out.println("termina");

	}
	
	@Async
	public void sendNotificationRequest(String to, String name, String bookTittle, String date) throws MailException, MessagingException {
		MailTemplateRequest mailTemplateRequest = new MailTemplateRequest();
		mailTemplateRequest.setTo(to);
		mailTemplateRequest.setSubject("Request successful - Library Spring Boot REST API");
		Context context = new Context();
		context.setVariable("name",name);
		context.setVariable("description","Your request for borrow the book "+bookTittle+" has been successful. Remember that you have until "+date+" to deliver the book.");
		mailTemplateRequest.setContext(context);
		mailTemplateRequest.setTemplate("template-1");
		mailService.sendEmailWithTemplate(mailTemplateRequest);
	}
	
	
	private void validatePageNumberAndSize(int page, int size) {
		
        if(page < 0) {
            throw new BadRequestException("Page number cannot be less than zero.");
        }

        if(size > AppConstants.MAX_PAGE_SIZE) {
            throw new BadRequestException("Page size must not be greater than " + AppConstants.MAX_PAGE_SIZE);
        }
        
    }
	
	public BorrowResponse convert(Borrow q) {
		BorrowResponse a = new BorrowResponse();
		a.setId(q.getId());
		BookPlainResponse bookPlainResponse = modelMapper.map(q.getBook(), BookPlainResponse.class);
		a.setBook(bookPlainResponse);
		a.setReceptionDate(q.getReceptionDate());
		a.setDeliveryDate(q.getDeliveryDate());
		a.setUser(userService.getUserById(q.getCreatedBy()));
		Date createdAt = Date.from(q.getCreatedAt());
		a.setCreatedAt(createdAt);
		a.setStatus(q.getStatus());
		return a;
	}
	
}
