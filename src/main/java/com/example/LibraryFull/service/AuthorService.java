package com.example.LibraryFull.service;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.LibraryFull.entity.Author;
import com.example.LibraryFull.entity.Book;
import com.example.LibraryFull.repository.AuthorRepository;
import com.example.LibraryFull.repository.BookRepository;
import com.example.LibraryFull.request.AuthorRequest;
import com.example.LibraryFull.response.AuthorResponse;
import com.example.LibraryFull.response.BookPlainResponse;
import com.example.LibraryFull.response.EditorialResponse;
import com.example.LibraryFull.response.PagedResponse;
import com.example.LibraryFull.response.PagedResponseWithEntity;
import com.example.LibraryFull.exception.BadRequestException;
import com.example.LibraryFull.util.AppConstants;
import com.example.LibraryFull.exception.ResourceNotFoundException;

@Service
public class AuthorService {
	
	@Autowired
    private AuthorRepository authorRepository;
	
	@Autowired
	private BookRepository bookRespository;
	
	@Autowired
	ModelMapper modelMapper;
	
	public AuthorResponse createAuthor(AuthorRequest authorRequest) {
		
		Author author = modelMapper.map(authorRequest, Author.class);
		authorRepository.save(author);
		AuthorResponse authorResponse = modelMapper.map(author, AuthorResponse.class);
		return authorResponse;
		
	}
	
	public AuthorResponse getAuthorById(Long authorId) {
		Author author = authorRepository.findById(authorId).orElseThrow(
                () -> new ResourceNotFoundException("Author", "id", authorId));
		
		AuthorResponse authorResponse = modelMapper.map(author, AuthorResponse.class);
		return authorResponse;

    }
	
	public Set<Author> getAuthorInId(Set<Long> ids) {
		return authorRepository.findByIdIn(ids);
	}
	
	public PagedResponse<AuthorResponse> getAuthors(int page, int size) {
        validatePageNumberAndSize(page, size);

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "id");
        Page<Author> authors = authorRepository.findAll(pageable);

        if (authors.getNumberOfElements() == 0) {
            return new PagedResponse<>(Collections.emptyList(), authors.getNumber(),
            		authors.getSize(), authors.getTotalElements(), authors.getTotalPages(), authors.isLast());
        }

        Type listType = new TypeToken<List<AuthorResponse>>(){}.getType();
		List<AuthorResponse> authorResponses = modelMapper.map(authors.getContent(),listType);

        return new PagedResponse<>(authorResponses, authors.getNumber(),
        		authors.getSize(), authors.getTotalElements(), authors.getTotalPages(), authors.isLast());
    }
	
	
	public AuthorResponse updateAuthor(AuthorRequest authorRequest, Long authorId) {
		Author authorCheck = authorRepository.findById(authorId).orElseThrow(
                () -> new ResourceNotFoundException("Author", "id", authorId));
		
		Author author = modelMapper.map(authorRequest, Author.class);
		author.setId(authorId);
		authorRepository.save(author);
		AuthorResponse authorResponse = modelMapper.map(author, AuthorResponse.class);
		
		return authorResponse;
	}
	
	/*
	public PagedResponseWithEntity<AuthorResponse,BookPlainResponse> getBooksByAuthor(Long authorId, int page, int size) {
        validatePageNumberAndSize(page, size);
      
        AuthorResponse authorResponse = getAuthorById(authorId);
        
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "id");
        Page<Book> books = bookRespository.findByAuthorId(authorId,pageable);
        
        
        
        
        if (books.getNumberOfElements() == 0) {
        	
        	return new PagedResponseWithEntity(authorResponse, Collections.emptyList(),books.getNumber(),
            		books.getSize(), books.getTotalElements(), books.getTotalPages(), books.isLast());
            
        }
        
        Type listType = new TypeToken<List<BookPlainResponse>>(){}.getType();
		List<BookPlainResponse> bookResponses = modelMapper.map(books.getContent(),listType);
		
		return new PagedResponseWithEntity(authorResponse, bookResponses,books.getNumber(),
        		books.getSize(), books.getTotalElements(), books.getTotalPages(), books.isLast());
        
    }
    
    */
	
	private void validatePageNumberAndSize(int page, int size) {
		
        if(page < 0) {
            throw new BadRequestException("Page number cannot be less than zero.");
        }

        if(size > AppConstants.MAX_PAGE_SIZE) {
            throw new BadRequestException("Page size must not be greater than " + AppConstants.MAX_PAGE_SIZE);
        }
        
    }
	
}
