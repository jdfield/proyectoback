package com.example.LibraryFull.service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.LibraryFull.entity.Author;
import com.example.LibraryFull.entity.Book;
import com.example.LibraryFull.entity.Editorial;
import com.example.LibraryFull.exception.BadRequestException;
import com.example.LibraryFull.exception.ResourceNotFoundException;
import com.example.LibraryFull.repository.BookRepository;
import com.example.LibraryFull.repository.EditorialRepository;
import com.example.LibraryFull.request.BookRequest;
import com.example.LibraryFull.response.AuthorResponse;
import com.example.LibraryFull.response.BookResponse;
import com.example.LibraryFull.response.PagedResponse;
import com.example.LibraryFull.util.AppConstants;


@Service
public class BookService {
	
	@Autowired
    BookRepository bookRepository;
	
	@Autowired
    private EditorialRepository editorialRepository;
	
	@Autowired
	AuthorService authorService;
	
	@Autowired
	ModelMapper modelMapper;
	
	public BookResponse createBook(BookRequest bookRequest) {
		
		Editorial editorial = editorialRepository.findById(bookRequest.getEditorialId()).orElseThrow(
                () -> new ResourceNotFoundException("Editorial", "id", bookRequest.getEditorialId()));
		
		Book book = modelMapper.map(bookRequest, Book.class);
		
		Set<Author> authorsSet = authorService.getAuthorInId(bookRequest.getAuthorIds());
		
		List<Author> authors = new ArrayList<>(authorsSet);
		
		book.setId(null); //tocó enviarlo porque tomaba el id de la editorial
		book.setEditorial(editorial);
		book.setAuthors(authors);
		
		bookRepository.save(book);
		
		BookResponse bookResponse = modelMapper.map(book, BookResponse.class);
		
		return bookResponse;
		
	}
	
	public PagedResponse<BookResponse> getBooks(int page, int size) {
        validatePageNumberAndSize(page, size);

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "id");
        Page<Book> books = bookRepository.findAll(pageable);

        if (books.getNumberOfElements() == 0) {
            return new PagedResponse<>(Collections.emptyList(), books.getNumber(),
            		books.getSize(), books.getTotalElements(), books.getTotalPages(), books.isLast());
        }

        Type listType = new TypeToken<List<BookResponse>>(){}.getType();
		List<BookResponse> bookResponse = modelMapper.map(books.getContent(),listType);

        return new PagedResponse<>(bookResponse, books.getNumber(),
        		books.getSize(), books.getTotalElements(), books.getTotalPages(), books.isLast());
    }
	
	public BookResponse getBookById(Long bookId) {
		Book book = bookRepository.findById(bookId).orElseThrow(
                () -> new ResourceNotFoundException("Book", "id", bookId));
		
		BookResponse bookResponse = modelMapper.map(book, BookResponse.class);
		return bookResponse;

    }
	
	public PagedResponse<BookResponse> getBooksAvailables(Boolean available, int page, int size) {
        validatePageNumberAndSize(page, size);

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "id");
        Page<Book> books = bookRepository.findByAvailable(available,pageable);

        if (books.getNumberOfElements() == 0) {
            return new PagedResponse<>(Collections.emptyList(), books.getNumber(),
            		books.getSize(), books.getTotalElements(), books.getTotalPages(), books.isLast());
        }

        Type listType = new TypeToken<List<BookResponse>>(){}.getType();
		List<BookResponse> bookResponse = modelMapper.map(books.getContent(),listType);

        return new PagedResponse<>(bookResponse, books.getNumber(),
        		books.getSize(), books.getTotalElements(), books.getTotalPages(), books.isLast());
    }
	
	public int setAvailableInBooks(Boolean available,Collection<Long> ids) {
		return bookRepository.setAvailableInBooks(available, ids);
	}
	
	public int setAvailableBooks(Boolean newAvailable, Boolean available) {
		return bookRepository.setAvailableBooks(newAvailable, available);
	}
	
	
	private void validatePageNumberAndSize(int page, int size) {
		
        if(page < 0) {
            throw new BadRequestException("Page number cannot be less than zero.");
        }

        if(size > AppConstants.MAX_PAGE_SIZE) {
            throw new BadRequestException("Page size must not be greater than " + AppConstants.MAX_PAGE_SIZE);
        }
        
    }
}
