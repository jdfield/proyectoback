package com.example.LibraryFull.service;


import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


import com.example.LibraryFull.entity.Editorial;
import com.example.LibraryFull.entity.Book;
import com.example.LibraryFull.exception.BadRequestException;
import com.example.LibraryFull.exception.ResourceNotFoundException;
import com.example.LibraryFull.repository.BookRepository;
import com.example.LibraryFull.repository.EditorialRepository;
import com.example.LibraryFull.request.EditorialRequest;
import com.example.LibraryFull.response.AuthorPlainResponse;
import com.example.LibraryFull.response.BookAuthorsResponse;
import com.example.LibraryFull.response.PagedResponseWithEntity;
import com.example.LibraryFull.response.EditorialResponse;
import com.example.LibraryFull.response.PagedResponse;
import com.example.LibraryFull.util.AppConstants;



@Service
public class EditorialService {
	
	@Autowired
    private EditorialRepository editorialRepository;
	
	@Autowired
	private BookRepository bookRespository;
	
	@Autowired
	ModelMapper modelMapper;
	
	public PagedResponse<EditorialResponse> getEditorials(int page, int size) {
        validatePageNumberAndSize(page, size);

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "id");
        Page<Editorial> editorials = editorialRepository.findAll(pageable);

        if (editorials.getNumberOfElements() == 0) {
            return new PagedResponse<>(Collections.emptyList(), editorials.getNumber(),
            		editorials.getSize(), editorials.getTotalElements(), editorials.getTotalPages(), editorials.isLast());
        }

        Type listType = new TypeToken<List<EditorialResponse>>(){}.getType();
		List<EditorialResponse> editorialResponses = modelMapper.map(editorials.getContent(),listType);

        return new PagedResponse<>(editorialResponses, editorials.getNumber(),
        		editorials.getSize(), editorials.getTotalElements(), editorials.getTotalPages(), editorials.isLast());
    }
	
	
	public EditorialResponse createEditorial(EditorialRequest editorialRequest) {
		
		Editorial editorial = modelMapper.map(editorialRequest, Editorial.class);
		editorialRepository.save(editorial);
		EditorialResponse editorialResponse = modelMapper.map(editorial, EditorialResponse.class);
		return editorialResponse;
		
	}
	
	public PagedResponseWithEntity<EditorialResponse,BookAuthorsResponse> getBooksByEditorial(Long editorialId, int page, int size) {
        validatePageNumberAndSize(page, size);
      
        EditorialResponse editorialResponse = getEditorialById(editorialId);
        
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "id");
        Page<Book> books = bookRespository.findByEditorialId(editorialId,pageable);
        
        
        
        
        if (books.getNumberOfElements() == 0) {
        	
        	return new PagedResponseWithEntity(editorialResponse, Collections.emptyList(),books.getNumber(),
            		books.getSize(), books.getTotalElements(), books.getTotalPages(), books.isLast());
           
        }
        
        Type listType = new TypeToken<List<BookAuthorsResponse>>(){}.getType();
		//List<BookAuthorsResponse> bookResponses = modelMapper.map(books.getContent(),listType);
		List<BookAuthorsResponse> bookResponses = new ArrayList<BookAuthorsResponse>();
		
		books.getContent().forEach(q -> bookResponses.add(convert(q)));


		return new PagedResponseWithEntity(editorialResponse, bookResponses,books.getNumber(),
        		books.getSize(), books.getTotalElements(), books.getTotalPages(), books.isLast());
        
    }
	
	public EditorialResponse getEditorialById(Long editorialId) {
		Editorial editorial = editorialRepository.findById(editorialId).orElseThrow(
                () -> new ResourceNotFoundException("Editorial", "id", editorialId));
		
		EditorialResponse editorialResponse = modelMapper.map(editorial, EditorialResponse.class);
		return editorialResponse;

    }
	
	private void validatePageNumberAndSize(int page, int size) {
		
        if(page < 0) {
            throw new BadRequestException("Page number cannot be less than zero.");
        }

        if(size > AppConstants.MAX_PAGE_SIZE) {
            throw new BadRequestException("Page size must not be greater than " + AppConstants.MAX_PAGE_SIZE);
        }
        
    }
	
	public BookAuthorsResponse convert(Book q) {
		BookAuthorsResponse a = new BookAuthorsResponse();
		a.setId(q.getId());
		a.setDescription(q.getDescription());
		a.setTitle(q.getTitle());
		a.setAvailable(q.getAvailable());
		Type listType = new TypeToken<List<AuthorPlainResponse>>(){}.getType();
		List<AuthorPlainResponse> authorResponses = modelMapper.map(q.getAuthors(),listType);
		a.setAuthors(authorResponses);
		
		return a;
	}

}
